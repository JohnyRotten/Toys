Rails.application.routes.draw do
  resources :users
  resources :items
  resources :categories
  resources :images,	only: [ :create, :new, :destroy ]
  resources :comments,	only: [ :create, :new, :destroy ]
  resources :reviews
  resources :faqs
  resources :sessions,	only: [ :new, :destroy, :create ]

  resources :carts,		only: [ :create, :destroy, :index ]
  resources :orders,	only: [ :create, :destroy ]
  resources :bookmarks,	only: [ :create, :destroy, :index ]

  root 'items#catalog'

  match '/about',		to: 'static_pages#about',		via: 'get'
  match '/contacts',	to: 'static_pages#contacts',	via: 'get'
  match '/help',		to: 'static_pages#help',		via: 'get'
  match '/catalog',		to: 'items#catalog',			via: 'get'

  match '/signin',		to: 'sessions#new',				via: 'get'
  match '/signout',		to: 'sessions#destroy',			via: 'delete'
  match '/signup',		to: 'users#new',				via: 'get'

  get 'sessions/create'

end
