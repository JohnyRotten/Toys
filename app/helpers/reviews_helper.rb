module ReviewsHelper
	def review_object
		review = Review.find_by(user_id: current_user.id)
		if review.nil?
			Review.new
		else
			review 
		end
	end
end
