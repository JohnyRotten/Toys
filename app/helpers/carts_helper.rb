module CartsHelper
	def cart_prices
		sum = 0
		current_user.carts.each do |x|
			sum += x.price
		end
		sum
	end
end
