module ItemsHelper
	def get_last_see
		Item.all[0..3]
	end

	def get_popular
		Item.all.sort { |x| x.bookmarks.count }[0..3]
	end
end
