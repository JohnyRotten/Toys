class User < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	has_many :carts,	dependent: :destroy
	has_many :orders,	dependent: :destroy
	has_many :bookmarks,	dependent: :destroy

	has_secure_password

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

	before_save { email.downcase! }
	before_create :create_remember_token

	validates :login, presence: true, length: { in: 3..50 }
	validates :email, presence: true, format: { with: VALID_EMAIL_REGEX },
					  uniqueness: { case_sensitive: false }

	validates :password, presence: true, length: { minimum: 6 }
	validates :password_confirmation, presence: true

	def in_cart?(item)
		carts.find { |x| x.item === item }
	end

	def cart_link(item)
		Cart.where(user_id: id, item_id: item.id).first
	end

	def in_orders?(item)
		orders.find { |x| x.item === item }
	end

	def order_link(item)
		Order.where(user_id: id, item_id: item.id).first
	end

	def in_bookmarks?(item)
		bookmarks.find { |x| x.item === item }
	end

	def bookmark_link(item)
		Bookmark.where(user_id: id, item_id: item.id).first
	end

	def User.new_remember_token
		SecureRandom.urlsafe_base64
	end

	def User.encrypt(token)
		Digest::SHA1.hexdigest(token.to_s)
	end

	private

		def create_remember_token
			self.remember_token = User.encrypt(User.new_remember_token)
		end
end
