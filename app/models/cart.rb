class Cart < ActiveRecord::Base
	belongs_to :user,	class_name: 'User'
	belongs_to :item,	class_name: 'Item'

	validates :item_id,	presence: true
	validates :user_id,	presence: true
	validates :price,	presence: true
end
