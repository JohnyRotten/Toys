class Item < ActiveRecord::Base
	has_many :images,	dependent: :destroy
	has_many :comments,	dependent: :destroy
	has_many :bookmarks,	dependent: :destroy
	belongs_to	:category

	validates :name,	presence: true, length: { in: 3..25 }
	validates :desc,	presence: true
	validates :price,	presence: true
	validates :category_id,	presence: true

	def image
		# return Image.new(path: '/images/image-not-found.jpg', title: 'Not found')
		return self.images.first if self.images.any?
		return Image.new
	end
end
