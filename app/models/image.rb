class Image < ActiveRecord::Base
	belongs_to :item

	has_attached_file :path, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
	validates_attachment_content_type :path, :content_type => /\Aimage\/.*\Z/

	# validates :path, presence: true, uniqueness: { case_sensitive: true }
	validates :title, presence: true
	validates :item_id,	presence: true

end