class Faq < ActiveRecord::Base
	validates :question,	presence: true, length: { in: 10...350 }
	validates :answer,		presence: true, length: { in: 10...350 }
end
