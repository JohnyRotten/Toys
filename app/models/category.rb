class Category < ActiveRecord::Base
	has_many :items,		dependent: :destroy
	belongs_to :category 
	has_many :categories,	dependent: :destroy

	validates :name,	presence: true,	uniqueness: true
	validates :desc,	presence: true

	before_save { self.category_id ||= 0 }
end
