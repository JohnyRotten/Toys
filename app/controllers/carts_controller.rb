class CartsController < ApplicationController
	before_action :signed_in_user

	def index
		@carts = current_user.carts
	end

	def create
		p = cart_params
		p[:price] = Item.find(p[:item_id]).price
		@cart = current_user.carts.build(p)
		if @cart.save
			flash[:success] = "Item added to cart."
		else
			flash[:error] = "Item don't added to cart."
		end
		redirect_to :back
	end

	def destroy
		Cart.find(params[:id]).destroy
		flash[:success] = "Item was removed from cart."
		redirect_to :back
	end

	private

		def cart_params
			params.require(:cart).permit(:item_id)
		end
end
