class ItemsController < ApplicationController
	include CategoriesHelper

	before_action :get_filter, only: [ :catalog, :index ]

	def index
		@items = Item.paginate(page: params[:page])
	end

	def show
		@item = Item.find(params[:id])
	end

	def new
		@item = Item.new
	end

	def create
		@item = Item.create(item_params)
		if @item.save
			flash[:success] = 'Item created.'
			redirect_to @item
		else
			flash.now[:error] = "Item don't created."
			render 'new'
		end
	end

	def edit
		@item = Item.find(params[:id])
	end

	def update
		@item = Item.find(params[:id])
		if @item.update_attributes item_params
			flash[:success] = 'Item updated.'
			redirect_to @item 
		else
			flash.now[:error] = "Item don't updated."
			render 'edit'
		end
	end

	def destroy
		Item.find(params[:id]).destroy
		flash[:success] = 'Item removed.'
		redirect_to items_path
	end

	def catalog
		@items = Item.paginate(page: params[:page], per_page: 12)
	end

	private

		def get_filter
			current_user
		end

		def item_params
			params.require(:item).permit(:name, :articul, :desc, :price, :category_id)
		end
end
