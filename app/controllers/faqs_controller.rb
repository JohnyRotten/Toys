class FaqsController < ApplicationController
	def index
		@faqs = Faq.all
	end

	def new
		@faq = Faq.new
	end

	def create
		@faq = Faq.create(faq_params)
		if @faq.save
			flash[:success] = 'F&Q created.'
		else
			flash[:error] = "F&Q don't created."
		end
		redirect_to faqs_path
	end

	def destroy
		Faq.find(params[:id]).destroy
		flash[:success] = 'F&Q removed.'
		redirect_to faqs_path
	end

	private

		def faq_params
			params.require(:faq).permit(:question, :answer)
		end
end
