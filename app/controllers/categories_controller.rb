class CategoriesController < ApplicationController
	before_action :admin_user, only: [ :index, :new, :create, :destroy ]

	def index

	end

	def show
		@category = Category.find(params[:id])
		@items = find_child(@category)#.paginate(page: params[:page])
	end

	private

		def find_child(cat)
			arr = cat.items
			cat.categories.each do |x|
				arr += find_child(x)
			end
			arr
		end
end
