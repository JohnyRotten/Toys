class UsersController < ApplicationController
  
  before_action :signed_in_user,  only: [ :edit,    :update,  :index ]
  before_action :correct_user,    only: [ :edit,    :update ]
  before_action :auth_user,       only: [ :new,     :create ]
  before_action :admin_user,      only: [ :destroy, :index ]

  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.create(user_params)
    if @user.save
      sign_in @user
      flash[:success] = 'User created'
      redirect_to @user 
    else
      flash.now[:error] = "User don't created"
      render 'new'
    end
  end

  def edit
  end

  def update
    if @user.update_attributes user_params
      flash[:success] = 'User updated'
      redirect_to @user
    else
      flash.now[:error] = "User don't updated"
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = 'User was removed.'
    redirect_to users_path
  end

  private

    def user_params
      params.require(:user).permit(:login, :email, :password, :password_confirmation)
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to root_url unless current_user?(@user)
    end

    def admin_user
      redirect_to root_url unless current_user.admin?
    end
end
