class ReviewsController < ApplicationController
	before_action :signed_in_user,	only: [ :create, :update ]

	def index
		@reviews = Review.all
	end

	def create
		p = review_params
		p[:user_id] = current_user.id
		@review = Review.create(p)
		if @review.save
			flash[:success] = "Review created."
		else
			flash[:error] = "Review don't created."
		end
		redirect_to reviews_path
	end

	def update
		@review = Review.find(params[:id])
		if @review.update_attributes review_params
			flash[:success] = "Review updated."
			redirect_to reviews_path
		else
			flash[:error] = "Review don't updated."
			render 'edit'
		end
	end

	def destroy
		Review.find(params[:id]).destroy
		flash[:success] = "Review removed."
		redirect_to reviews_path
	end

	private

		def review_params
			params.require(:review).permit(:title, :message, :mark)
		end
end
