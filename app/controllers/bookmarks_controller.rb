class BookmarksController < ApplicationController
	before_action :signed_in_user

	def index
		@bookmarks = current_user.bookmarks
	end

	def create
		@bookmark = current_user.bookmarks.build(bookmark_params)
		if @bookmark.save
			flash[:success] = "Bookmark was added."
		else
			flash[:error] = "Bookmarj don't added."
		end
		redirect_to :back
	end

	def destroy
		Bookmark.find(params[:id]).destroy
		flash[:success] = "Bookmark was removed."
		redirect_to :back
	end

	private

		def bookmark_params
			params.require(:bookmark).permit(:item_id)
		end
end
