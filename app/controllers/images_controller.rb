class ImagesController < ApplicationController
	before_action :admin_user

	def create
		@image = Image.create(image_params)
		if @image.save
			flash[:success] = "Image was added."
		else
			flash[:error] = "Image don't create."
		end
		redirect_to edit_item_path(@image.item)
	end

	def destroy
		image = Image.find(params[:id])
		item_id = image.item.id
		image.path = nil
		image.save!
		image.destroy
		flash[:success] = "Image was removed."
		redirect_to edit_item_path(Item.find(item_id))
	end

	private

		def image_params
			params.require(:image).permit(:path, :title, :item_id)
		end
end
