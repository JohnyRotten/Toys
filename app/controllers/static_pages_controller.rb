class StaticPagesController < ApplicationController
  def home
  end

  def about
  end

  def contacts
  end

  def help
  end

  def blog
  end
end
