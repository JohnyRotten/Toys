class CommentsController < ApplicationController
	before_action :signed_in_user, only: :create

	def create
		@comment = current_user.comments.build(comment_params)
		if @comment.save
			flash[:success] = 'Comment was added.'
		else
			flash[:error] = "Comment don't added."
		end
		redirect_to @comment.item
	end

	private

		def comment_params
			params.require(:comment).permit(:message, :item_id)
		end
end
