User.create login: 'admin', email: 'admin@example.com', admin: true, password: '0GTPjX', password_confirmation: '0GTPjX'

1.upto(10) do |x|
	User.create login: "User #{x}", email: "user_#{x}@example.com", password: "user_#{x}", password_confirmation: "user_#{x}"
end

category1 = Category.create(name: 'Игрушки', desc: 'Игрушки')
category2 = category1.categories.create!(name: 'Мягкие игрушки', desc: 'Мягкие игрушки')
category3 = category1.categories.create!(name: 'Твердые игрушки', desc: 'НеМягкие игрушки')
category4 = Category.create(name: 'Куклы', desc: 'Куклы')
category5 = Category.create(name: 'Прочее', desc: 'Прочее')
# category6 = category2.categories.create!(name: 'Test 1', desc: 'Test 1')
# category7 = category2.categories.create!(name: 'Test 2', desc: 'Test 2')

item1 = category2.items.create!(name: 'Дарина', desc: 'Описание 1', price: 6000.0)
# item1.images.create!(path: '/uploaded/darina.jpg', title: 'Дарина')
item2 = category4.items.create!(name: 'Стефани', desc: 'Test1', price: 3000.0)
# item2.images.create!(path: '/uploaded/stef1.jpg', title: 'Test')
# item2.images.create!(path: '/uploaded/stef2.jpg', title: 'Test')
item3 = category5.items.create!(name: 'Test', desc: 'Test', price: 23000)
# item3.images.create!(path: '/uploaded/test1.jpg', title: 'Test')
# item3.images.create!(path: '/uploaded/test2.jpg', title: 'Test')
item4 = category2.items.create!(name: 'Octodex', desc: 'Symbol open source coding community - Octodex', price: 10000)
# item4.images.create!(path: '/uploaded/octodex1.jpg', title: 'Octodex')
# item4.images.create!(path: '/uploaded/octodex2.png', title: 'Octodex')
# item4.images.create!(path: '/uploaded/octodex3.jpg', title: 'Octodex')
item5 = category5.items.create!(name: 'Undefined', desc: 'Undefined', price: 0)

1.upto(20) do |x|
	category5.items.create! name: "Item #{x}", desc: "Item #{x}", price: (x**x * 100 / Math.sqrt(x))
end