class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.integer :user_id
      t.string :title
      t.text :message
      t.integer :mark

      t.timestamps null: false
    end
  end
end
