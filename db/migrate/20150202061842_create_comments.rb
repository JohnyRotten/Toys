class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :user_id
      t.text :message
      t.integer :item_id

      t.timestamps null: false
    end
  end
end
